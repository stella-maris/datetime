<?php
/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTimeTest;

use Org_Heigl\DateTime\Time;
use PHPUnit\Framework\TestCase;

class TimeTest extends TestCase
{

    /**
     * @dataProvider formatProvider
     * @testdox Formatting $time as $format
     * @covers \Org_Heigl\DateTime\Time::format
     * @uses \Org_Heigl\DateTime\Time::__construct
     * @uses \Org_Heigl\DateTime\Time::fromDateTime
     * @uses \Org_Heigl\DateTime\Time::fromTime
     */
    public function testFormat(string $time, string $format, string $result)
    {
        $time = Time::fromTime($time);

        self::assertEquals($result, $time->format($format));
    }

    public function formatProvider(): array
    {
        return [
            ['01:01:01', 'H:i:s', '01:01:01'],
            ['01:01:01', '\H:i:s', 'H:01:01'],
            ['01:01:01', 'H:\i:s', '01:i:01'],
            ['01:01:01', 'H:i:\s', '01:01:s'],
            ['13:01:01', 'h:i:s', '01:01:01'],
            ['01:01:01', 'G:i:s', '1:01:01'],
            ['13:01:01', 'G:i:s', '13:01:01'],
            ['13:01:01', 'g:i:s', '1:01:01'],
        ];
    }
}
