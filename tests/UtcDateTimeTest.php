<?php
/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTimeTest;

use DateTimeImmutable;
use DateTimeZone;
use Org_Heigl\DateTime\UtcDateTime;
use PHPUnit\Framework\TestCase;

class UtcDateTimeTest extends TestCase
{
    /**
     * @covers \Org_Heigl\DateTime\UtcDateTime::__construct
     */
    public function testInstnatiationWorksAsExpected(): void
    {
        $dt = new UtcDateTime('05.05.2022 12:23:34.567', new DateTimeZone('Europe/Berlin'));

        self::assertInstanceof(UtcDateTime::class, $dt);
        self::assertEquals(new DateTimeZone('UTC'), $dt->getTimezone());
        self::assertEquals('2022-05-05 10:23:34.567000', $dt->format('Y-m-d H:i:s.u'));
    }

    /**
     * @covers \Org_Heigl\DateTime\UtcDateTime::setTimezone
     */
    public function testCallingFunctionReturnsnNewUtcDateTimeInstance(): void
    {
        $dt = new UtcDateTime();

        $dt1 = $dt->setTimezone(new DateTimeZone('Europe/Berlin'));

        self::assertInstanceof(UtcDateTime::class, $dt1);
    }
}
