<?php
/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTimeTest;

use DateTimeZone;
use Org_Heigl\DateTime\Date;
use Org_Heigl\DateTime\DateTime;
use Org_Heigl\DateTime\Time;
use PHPUnit\Framework\TestCase;

class ReadmeTest extends TestCase
{
    public function testUsageExample(): void
    {
        $date = Date::fromDate('12.3.2022');
        self::assertSame('2022-03-12', $date->format('Y-m-d'));

        $time = Time::fromTime('12:34:45');
        self::assertSAme('12-34-45', $date->format('H-i-s'));

        $datetime = DateTime::fromDateTimeAndTimezone($date, $time, new DateTimeZone('Europe/Busingen'));
        self::assertSAme('2022-03-12 12:34:45', $datetime->format('Y-m-d H:i:s', new DateTimeZone('America/New_York')));
    }
}
