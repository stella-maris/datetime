<?php

declare(strict_types=1);

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTime;

use DateTimeImmutable;
use DateTimeZone;
use Org_Heigl\DateTime\Exception\InvalidTimezone;
use RuntimeException;

class DateTime extends DateTimeImmutable
{
    public static function fromDateTimeAndTimezone(Date $date, Time $time, DateTimeZone $timezone): DateTime
    {
        return new self(
            $date->format('Y-m-d') . 'T' . $time->format('H:i:s.u'),
            $timezone
        );
    }

    public function format(string $format, ?DateTimeZone $inTimezone = null): string
    {
        if (null === $inTimezone) {
            $inTimezone = $this->getTimezone();
        }
        if (false === $inTimezone) {
            throw InvalidTimezone::noTimezoneAvailable();
        }
        return $this->setTimezone($inTimezone)->format($format);
    }
}