<?php

declare(strict_types=1);

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTime;

use DateTimeImmutable;
use DateTimeZone;

class UtcDateTime extends DateTimeImmutable
{
    public function __construct(string $datetime = "now", ?DateTimeZone $timezone = null)
    {
        $utc = new DateTimeZone('UTC');
        $f = new DateTimeImmutable($datetime, $timezone);
        parent::__construct($f->setTimezone($utc)->format('Y-m-d H:i:s.u'), $utc);
    }
}