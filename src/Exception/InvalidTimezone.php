<?php

declare(strict_types=1);

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTime\Exception;

use RuntimeException;

class InvalidTimezone extends RuntimeException
{
    public static function noTimezoneAvailable(): self
    {
        return new self(sprintf(
            'There is no valid timezone available'
        ));
    }
}