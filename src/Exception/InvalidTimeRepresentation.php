<?php

declare(strict_types=1);

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTime\Exception;

class InvalidTimeRepresentation extends \RuntimeException
{
    public static function fromRegularExpression(string $timeString): self
    {
        return new self(sprintf(
            'The string "%s" seems not to be in the expected format (HH:MM:SS)',
            $timeString
        ));
    }

    public static function hourOverflowing(int $hour): self
    {
        return new self(sprintf(
            'The provided value for the hour (%1$02d) is outside the valid range for one day',
            $hour
        ));
    }

    public static function minuteOverflowing(int $minute): self
    {
        return new self(sprintf(
            'The provided value for the minute (%1$02d) is outside the valid range for one hour',
            $minute
        ));
    }

    public static function secondOverflowing(int $second): self
    {
        return new self(sprintf(
            'The provided value for the second (%1$02d) is outside the valid range for one minute',
            $second
        ));
    }
}