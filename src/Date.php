<?php

declare(strict_types=1);

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTime;

use DateTimeImmutable;
use DateTimeInterface;
use DateInterval;
use function gregoriantojd;

class Date
{
    private function __construct(private int $juliandate) {}

    public static function now(): self
    {
        return self::fromDateTime(new DateTimeImmutable());
    }

    public static function fromDateTime(DateTimeInterface $dateTime): self
    {
        return new self(gregoriantojd(
            (int) $dateTime->format('m'),
            (int) $dateTime->format('d'),
            (int) $dateTime->format('Y')
        ));
    }

    public static function fromDate(string $dateString): self
    {
        return self::fromDateTime(new DateTimeImmutable($dateString . 'T12:00:00'));
    }

    public function add(int $days): self
    {
        return new self($this->juliandate + $days);
    }

    public function sub(int $days): self
    {
        return new self($this->juliandate - $days);
    }

    public function diff(Date $date): DateInterval
    {
        return new DateInterval('P' . ($this->juliandate - $date->juliandate) . 'D');
    }

    public function format(string $format): string
    {

        return (new DateTimeImmutable(jdtojulian($this->juliandate) . ' 12:00:00'))->format($format);
    }
}