<?php

declare(strict_types=1);

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\DateTime;

use DateInterval;
use DateTimeImmutable;
use Org_Heigl\DateTime\Exception\InvalidTimeRepresentation;

class Time
{
    private function __construct(private int $secondsSinceNewDay) {}

    public static function now(): self
    {
        return self::fromDateTime(new DateTimeImmutable());
    }

    public static function fromDateTime(DateTimeImmutable $dateTime):self
    {
        $midnight = $dateTime->modify('midnight');

        return new self($dateTime->getTimestamp() - $midnight->getTimestamp());
    }

    public static function fromTime(string $timeString): self
    {
        if (! preg_match('/(?P<hour>[\d]{1,2}):(?P<minute>[\d]{1,2}):(?P<second>[\d]{1,2})/', $timeString, $result)) {
            throw InvalidTimeRepresentation::fromRegularExpression($timeString);
        }

        $hour = (int) $result['hour'];
        $minute = (int) $result['minute'];
        $second = (int) $result['second'];

        // Check that the time range is only between 00:00:00 and 23:59:59
        // We do not handle leap-seconds or times outside that range due to DST
        // as we are representing the time based on UTC, not local time as we do
        // not have timezone information available!
        if ($hour < 0 || $hour >=24) {
            throw InvalidTimeRepresentation::hourOverflowing($hour);
        }
        if ($minute < 0 || $minute >=59) {
            throw InvalidTimeRepresentation::minuteOverflowing($minute);
        }
        if ($second < 0 || $second >=59) {
            throw InvalidTimeRepresentation::secondOverflowing($second);
        }

        $time = (new DateTimeImmutable())->setTime($hour, $minute, $second);

        return self::fromDateTime($time);
    }

    public function addSeconds(int $seconds): self
    {
        return new self($this->secondsSinceNewDay + $seconds);
    }

    public function addMinutes(int $minutes): self
    {
        return $this->addSeconds($minutes * 60);
    }

    public function addHours(int $hours): self
    {
        return $this->addMinutes($hours * 60);
    }

    public function add(DateInterval $interval): self
    {
        return $this->addHours($interval->h)->addMinutes($interval->i)->addSeconds($interval->s);
    }

    public function subSeconds(int $seconds): self
    {
        return new self($this->secondsSinceNewDay - $seconds);
    }

    public function subMinutes(int $minutes): self
    {
        return $this->subSeconds($minutes * 60);
    }

    public function subHours(int $hours): self
    {
        return $this->subMinutes($hours * 60);
    }

    public function sub(DateInterval $interval): self
    {
        return $this->subHours($interval->h)->subMinutes($interval->i)->subSeconds($interval->s);
    }

     public function diff(Time $time): DateInterval
     {
         $diff = $time->secondsSinceNewDay - $this->secondsSinceNewDay;
         if ($diff < 0) {
             $diff = $diff * -1;
         }
         $hours = $diff % 3600;
         $minutes = ($diff - $hours * 3600) % 60;
         $seconds = ($diff - $hours * 3600) - ($minutes * 60);

         return new DateInterval(sprintf(
             'PT%dH%dM%dS',
             $hours,
             $minutes,
             $seconds
         ));
     }

     public function format(string $formatstring): string
     {
         preg_match_all('/(?<!\\\\)[aABgGhHisU]/', $formatstring, $results, PREG_OFFSET_CAPTURE + PREG_SET_ORDER);

         $results = array_reverse($results);
         foreach ($results as $result) {
             $timePart = '';
             switch ($result[0][0]) {
                 case 'a':
                     $timePart = 'pm';
                     if ((floor(($this->secondsSinceNewDay / (12 * 3600))) % 2) === 0) {
                         $timePart = 'am';
                     }
                     break;
                 case 'A':
                     $timePart = 'PM';
                     if ((floor(($this->secondsSinceNewDay / (12 * 3600))) % 2) === 0) {
                         $timePart = 'AM';
                     }
                     break;
                 case 'B':
                     $timePart = 1000 / 86400 * ($this->secondsSinceNewDay % 86400);
                     break;
                 case 'g':
                     $timePart = sprintf('%d', floor($this->secondsSinceNewDay / 3600) % 12);
                     break;
                 case 'G':
                     $timePart = sprintf('%d', floor($this->secondsSinceNewDay / 3600) % 24);
                     break;
                 case 'h':
                     $timePart = sprintf('%02d', floor($this->secondsSinceNewDay / 3600) % 12);
                     break;
                 case 'H':
                     $timePart = sprintf('%02d', floor($this->secondsSinceNewDay / 3600) % 24);
                     break;
                 case 'i':
                     $timePart = sprintf('%02d', floor (($this->secondsSinceNewDay % 3600) / 60));
                     break;
                 case 's':
                     $timePart = sprintf('%02d', ($this->secondsSinceNewDay % 60));
                     break;
                 case 'U':
                     $timePart = sprintf('%d', $this->secondsSinceNewDay);
                     break;
             }

             $formatstring = mb_substr($formatstring, 0, $result [0][1])
                 . $timePart
                 . mb_substr($formatstring, $result[0][1] + 1);
         }

         return str_replace('\\', '', $formatstring);
     }

     public function compare(Time $time): int
     {
         return $this->secondsSinceNewDay <=> $time->secondsSinceNewDay;
     }

     public function equals(Time $time): bool
     {
         return $this->secondsSinceNewDay === $time->secondsSinceNewDay;
     }
}