# DateTime

Date and Time functionality for PHP

This library contains some additions to the default DateTime fuctionality that
comes with PHP.

That is mainly using plain Date or plain Time objects as well as handling 
Dates and times outside the default Gregorian Calendar

## Installation

```bash
composer require org_heigl/datetime
```

## Usage

```php
use Org_Heigl\DateTime\Date;
use Org_Heigl\DateTime\DateTime;
use Org_Heigl\DateTime\Time;

$date = Date::fromDate('12.3.2022');
$date->format('Y-m-d');
// outputs 2022-03-12

$time = Time::fromTime('12:34:45');
$date->format('H-i-s')
// outputs 12-34-45

$datetime = DateTime::fromDateTimeAndTimezone($date, $time, new DateTimeZOne('Europe/Busingen'));
$datetime->format('Y-m-d H:i:s', new DateTimeZone('America/New_York'));
// output: 2022-03-12 12:34:45
```

### Date

If you only need a Date without a time (and without a timezone) you can 
create one like this:

```php
use Org_Heigl\DateTime\Date;
use Org_Heigl\DateTime\Time;

$date = Date::now();
// $date will reference the current day (based on the default timezone)
$time = Time::now();
// $time will reference the current time (based on the default timezone)
```

You can also reference a specific date or time like this:

```php
use Org_Heigl\DateTime\Date;
use Org_Heigl\DateTime\Time;

$date = Date::fromDate('2020-12-30');
// $date will reference December 30th 2020
$time = Time::fromTime('12:23:34.234567');
// $time will reference 12:23, 34 seconds and 234567 microseconds
```

Or you can create a time or a date from an object implementing the
DateTimeInterface.

```php
use Org_Heigl\DateTime\Date;
use Org_Heigl\DateTime\Time;

$dateTime = new DateTimeImmutable('2020-12-30T12:23:34.234567+02:00');
$date = Date::fromDateTime($dateTime);
// $date will reference December 30th 2020
$time = Time::fromDateTime($dateTime);
// $time will reference 12:23, 34 seconds and 234567 microseconds
```

The Date object implement the following methods:

* `Date::add(int $days): Date` to add a number of days
* `Date::sub(int $days): Date` to remove a number of days
* `Date::diff(Date $otherDate): DateInterval` to calculate the number of days between two dates.
  Note that this does not take into account any Timezone or calendar related oddities like the missing 
  30th of September 2011.
* `Time::diff(Time $otherTime): DateInterval` to calculate the time difference between two 
  times. The calculation does not take into account any timezone or DST changes.
  
## Install

Install the package via composer:

```bash
composer require org_heigl/datetime
```